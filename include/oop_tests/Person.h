// Person Header with function declarations

#include <iostream>
#include <string>

using namespace std;

#ifndef PERSON_H
#define PERSON_H

class Person {
public:
    //Default construct
    Person();

    //Overload Constructor for other input
    Person(string, string, string, int, int, double);

    //Destructor
    ~Person();

    //Begin Getters AKA Accessor Functions
    string getFirstName() const;
        //@return string - first name

    string getLastName() const;
        //@return string - last name
    
    string getiColour() const;
        //@return string - eye colour

    int getPersonAge() const;
        //@return int - Age
    
    int getPersonHeight() const;
        //@return int - Height in cm

    double getPersonWeight() const;
        //@return double - weight in pounds

    // Begin Setters AKA Mutator Functions
    void setFirstName(string);
        //@return string - first name

    void setLastName(string);
        //@return string - last name
    
    void setiColour(string);
        //@return string - eye colour

    void setPersonAge(int);
        //@return int - Age
    
    void setPersonHeight(int);
        //@return int - Height in cm

    void setPersonWeight(double);
        //@return double - weight in pounds

private:
    //Member variables
    string firstName;
    string lastName;
    string iColour;

protected:
    int personAge;
    int personHeight;
    double personWeight;
};

#endif //PERSON_H
