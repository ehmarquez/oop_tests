// Header with function declarations
// Child of Person Class
// Person has a BMI calculation

#include <iostream>
#include <string>
#include "Person.h"

using namespace std;

#ifndef BMI_H
#define BMI_H

class BMI : public Person {
public:
    //Default constructor similar to Python init for classes?
    BMI();

    //Overload Constructor
    BMI(int, double);

    /* Commented but keeping for future reference

    //Accessor Functions (Getter)
    string getName() const;     //use const if not modifying any private member
        //@return string - returns name of member

    int getHeight() const;
        //@return int - returns height of member

    double getWeight() const;
        //@return double - returns weight of member

    //Mutator Functions (Setter)
    void setName(string);       //parameter is input type not var name
        //sets name of member
        //@param string - name

    void setHeight(int);
        //sets height of member
        //@param int - height in cm

    void setWeight(double);
        //sets weight of a member
        //@param double - weight in lbs

    */ 

    double getResultBmi() const;
        //@return double

    void setResultBmi(double);
        // sets result BMI to calculated BMI
        // @param double - kg/meter squared

    double calcBMI();
        //Calculate BMI using member variables
        //@return double - BMI

    //Destructor
    ~BMI();

private:
    //Member variables

    double resultBmi;

    /* Should no longer need name, height, and weight
    since inherited from Person class 
    string newName;
    int newHeight;
    double newWeight;
    */
};

#endif
