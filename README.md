# Practice Repo #

### What is this repository for? ###

This repo tests some basic fundamentals for OOP using C++.
It is setup to act as a ROS package and run using rosrun command.

The main objective for this project is to help understand the connections in a family tree
(such as uncle/aunt, niece/nephew, 1st cousin, 2nd cousin) while also practicing OOP.

I find myself always getting confused with the family tree connections and figured this 
would help me in understanding the proper terms easier.

### How do I get set up? ###

Navigate to your ROS workspace src folder ($cd my_ros_workspace/src)

    git clone https://ehmarquez@bitbucket.org/ehmarquez/oop_tests.git

Navigate to your home workspace and install dependencies

    rosdep install --from-paths src --ignore-src --rosdistro=indigo -y

Compile the stack:

    $cd ~/my_ros_workspace
    $catkin_make

### Contact ###

Contact info: alvinmarquez7@gmail.com