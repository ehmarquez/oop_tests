#include <iostream>
#include <string>

#include "../include/oop_tests/BMI.h"
//#include "funcInput.cpp"

using namespace std;

int main() {

    //Using overloaded constructor
    BMI Member_1(176, 170);

    cout << endl << "Member Name: " << Member_1.getLastName() << endl <<
    "Height: " << Member_1.getPersonHeight() << endl <<
    "Weight: " << Member_1.getPersonWeight() << endl <<
    "BMI: " << Member_1.calcBMI() << endl;

    /*
    BMI Member_2;

    Member_2.setName("Cha");

    cout << endl << "Member Name: " << Member_2.getName() << endl;
    */

    return 0;
}
