//Person Function Definitions

#include "../include/oop_tests/Person.h"

Person::Person() {
    //Default construct has same Family Name and eye Colour
    lastName = "Marquez";
    iColour = "Black";
    cout << endl << "A new Person has been created!" << endl;
}

Person::Person(string fname, string lname, string colour, int age, int height, double weight) {
    firstName = fname;
    lastName = lname;
    iColour = colour;
    personAge = age;
    personHeight = height;
    personWeight = weight;
}

Person::~Person() {
    cout << endl << "Destructing person..." << endl;
}

//Begin getters
string Person::getFirstName() const {
    return firstName;
}

string Person::getLastName() const {
    return lastName;
}

string Person::getiColour() const {
    return iColour;
}

int Person::getPersonAge() const {
    return personAge;
}

int Person::getPersonHeight() const {
    return personHeight;
}

double Person::getPersonWeight() const {
    return personWeight;
}

//Begin setters
void Person::setFirstName(string fname)  {
    firstName = fname;
}

void Person::setLastName(string lname)  {
    lastName = lname;
}

void Person::setiColour(string colour)  {
    iColour = colour;
}

void Person::setPersonAge(int age)  {
    personAge = age;
}

void Person::setPersonHeight(int height)  {
    personHeight = height;
}

void Person::setPersonWeight(double weight)  {
    personWeight = weight;
}