//Function definitions

#include "../include/oop_tests/BMI.h"
#include "../include/oop_tests/Person.h"

BMI::BMI() {
}

//Keeping constructors and member initialization
BMI::BMI(int height, double weight)
    : Person()
    {
        setPersonHeight(height);
        setPersonWeight(weight);
    }

BMI::~BMI() {}

/* Commented but keeping for future reference

string BMI::getName() const {
    return newName;
}

int BMI::getHeight() const {
    return newHeight;
}

double BMI::getWeight() const {
    return newWeight;
}

void BMI::setName(string name) {
    newName = name;
}

void BMI::setHeight(int height) {
    newHeight = height;
}

void BMI::setWeight(double weight) {
    newWeight = weight;
}

*/

double BMI::getResultBmi() const {
    return resultBmi;
}

void BMI::setResultBmi(double personBmi) {
    resultBmi = personBmi;
}

double BMI::calcBMI() {
    setResultBmi((getPersonWeight() / 2.20462 / ((getPersonHeight()*getPersonHeight())/10000)));
    return getResultBmi();
}
